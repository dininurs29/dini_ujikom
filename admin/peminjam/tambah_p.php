<?php
include "header_p.php";
include "koneksi.php";
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title></title>
</head>
<body>
<div class="row">  
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top: 30px; margin-bottom: 30px; margin-left: 255px;">
        <div class="sparkline16-list responsive-mg-b-30">
            <div class="sparkline16-hd">
                <div class="main-sparkline16-hd">
                    <h1>Form Peminjaman</h1>
                    <form action="proses_peminjaman.php" method="POST" id="PrimaryModalhdbgcl">
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Nama Peminjam</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <input name="nm_peminjam" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Jenis</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <div class="chosen-select-single mg-b-20">
                                                                    <select class="form-control custom-select-value" name="id_inventaris">
                                                                            <option value="" disabled selected>Pilih</option>

                                                                            <?php
                                                                            $pilih=mysqli_query($konek, "SELECT * FROM inventaris");
                                                                            while($tampil=mysqli_fetch_array($pilih)){
                                                                            ?>
                                                                            <option value="<?=$tampil['id_inventaris'];?>"><?=$tampil['nama'];?></option>
                                                                              <?php
                                                                              }
                                                                              ?>
                                                                        </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Jumlah</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <input name="jumlah" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                            
                                                <button type="submit" name="tambah" class="btn btn-primary">Tambah</button>
                                                </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html> 
    <?php
    include "footer_p.php";
    ?>