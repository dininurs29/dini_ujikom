<?php
include "header.php";
include 'koneksi.php';
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Ujikom | Dini Nursafitri</title>
</head>

<body>
    <!-- Tambah Data -->
    <div class="row">
        <div id="PrimaryModalhdbgcl" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-color-modal bg-color-1">
                        <h5 class="modal-title">Data Inventaris</h5>
                        <div class="modal-close-area modal-close-df">
                            <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="sparkline12-graph">
                                <div class="basic-login-form-ad">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="all-form-element-inner">
                                                <form action="proses_data_barang.php" method="POST" id="PrimaryModalhdbgcl">
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Nama Barang</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <input name="nama" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Kondisi</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <input name="kondisi" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Keterangan</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <input name="keterangan" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Jumlah</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <input name="jumlah" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Jenis</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <div class="chosen-select-single mg-b-20">
                                                                    <select class="form-control custom-select-value" name="id_jenis">
                                                                            <option value="" disabled selected>Pilih</option>

                                                                            <?php
                                                                            $pilih=mysqli_query($konek, "SELECT * FROM jenis");
                                                                            while($tampil=mysqli_fetch_array($pilih)){
                                                                            ?>
                                                                            <option value="<?=$tampil['id_jenis'];?>"><?=$tampil['nama_jenis'];?></option>
                                                                              <?php
                                                                              }
                                                                              ?>
                                                                        </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Tanggal</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                <input name="tanggal_register" type="date"  class="form-control"  />
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Ruangan</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <div class="chosen-select-single mg-b-20">
                                                                    <select class="form-control custom-select-value" name="id_ruang">
                                                                            <option value="" disabled selected>Pilih</option>

                                                                            <?php
                                                                            $pilih=mysqli_query($konek, "SELECT * FROM ruang");
                                                                            while($tampil=mysqli_fetch_array($pilih)){
                                                                            ?>
                                                                            <option value="<?=$tampil['id_ruang'];?>"><?=$tampil['nama_ruang'];?></option>
                                                                              <?php
                                                                              }
                                                                              ?>
                                                                        </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Kode</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <?php
                                                                $koneksi=mysqli_connect("localhost","root","","ujikom");
                                                                $cari_kd =mysqli_query($konek,"SELECT kode_inventaris from inventaris order by id_inventaris desc");
                                                                $data = mysqli_fetch_array($cari_kd)
                                                                ?>
                                                                <input name="kode_inventaris" type="text" class="form-control" value="<?php echo substr($data['kode_inventaris'],4,1)+1?>" readonly/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Petugas</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <div class="chosen-select-single mg-b-20">
                                                                    <select class="form-control custom-select-value" name="id_petugas">
                                                                            <option value="" disabled selected>Pilih</option>

                                                                            <?php
                                                     $pilih=mysqli_query($konek, "SELECT * FROM petugas");
                                                     while($tampil=mysqli_fetch_array($pilih)){
                                                                            ?>
                                                                            <option value="<?=$tampil['id_petugas'];?>"><?=$tampil['nama_petugas'];?></option>
                                                                              <?php
                                                                              }
                                                                              ?>
                                                                        </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <button type="submit" name="tambah" class="btn btn-primary">Tambah</button>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <!-- <div class="modal-footer">
                        <div class="modal-area-button">
                            </div>
                        
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <!-- Selesai Tambah Data --> 
           <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px;">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="modal-area-button">
                                <a class="Primary mg-b-10" href="#" data-toggle="modal" data-target="#PrimaryModalhdbgcl">Tambah</a>
                                
                            </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <div id="toolbar">
                                        <select class="form-control dt-tb">
                                            <option value="">Export Basic</option>
                                            <option value="all">Export All</option>
                                            <option value="selected">Export Selected</option>
                                        </select>
                                    </div>
                                    <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                        <tr>
                                            <th data-field="">NO</th>
                                            <th data-field="nama" data-editable="true">Nama Barang</th>
                                            <th data-field="kondisi" data-editable="true">Kondisi</th>
                                            <th data-field="keterangan" data-editable="true">Keterangan</th>
                                            <th data-field="jumlah">Jumlah</th>
                                            <th data-field="jenis">Jenis</th>
                                            <th data-field="tanggal_register" data-editable="true">Tanggal Regis</th>
                                            <th data-field="ruangan" data-editable="true">Ruangan</th>
                                            <th data-field="kode_inventaris" data-editable="true">Kode</th>
                                            <th data-field="petugas" data-editable="true">Petugas</th>
                                            <th data-field="action">Action</th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                            <?php
                                            $nomer=1;
                                             $pilih=mysqli_query($konek,"SELECT * FROM inventaris i join jenis j ON i.id_jenis=j.id_jenis join ruang r ON i.id_ruang=r.id_ruang join petugas p ON i.id_petugas=p.id_petugas order by id_inventaris desc");
                                              while($tampil=mysqli_fetch_array($pilih)){
                                            ?>
                                            <tr>
                                            <td><?php echo $nomer++;?></td>
                                            <td><?php echo $tampil['nama'];?></td>
                                            <td><?php echo $tampil['kondisi'];?></td>
                                            <td><?php echo $tampil['keterangan'];?></td>
                                            <td><?php echo $tampil['jumlah'];?></td>
                                            <td><?php echo $tampil['nama_jenis'];?></td>
                                            <td><?php echo $tampil['tanggal_register'];?></td>
                                            <td><?php echo $tampil['nama_ruang'];?></td>
                                            <td><?php echo $tampil['kode_inventaris'];?></td>
                                            <td><?php echo $tampil['nama_petugas'];?></td>
                                            <td>
                                                <a href="hapus_data.php?id=<?=$tampil['id_inventaris'];?>"><button type="button" class="btn btn-danger"><i class="fa fa-trash edu-avatar" aria-hidden="true"></i></button></a>

                                                <a href="edit_data.php?id=<?=$tampil['id_inventaris'];?>"><button type="button" class="btn btn-primary"><i class="fa fa-pencil edu-graph-round" aria-hidden="true"></i></button></a>
                                            </td>
                                            </tr>

                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

</body>
    <?php
    include "footer.php";
    ?>
</html>