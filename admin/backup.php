<?php
$connect = new PDO("mysql:host=localhost;dbname=ujikom", "root", "");
$get_all_table_query = "SHOW TABLES";
$statement = $connect->prepare($get_all_table_query);
$statement->execute();
$result = $statement->fetchAll();

if(isset($_POST['table']))
{
 $output = '';
 foreach($_POST["table"] as $table)
 {
  $show_table_query = "SHOW CREATE TABLE " . $table . "";
  $statement = $connect->prepare($show_table_query);
  $statement->execute();
  $show_table_result = $statement->fetchAll();

  foreach($show_table_result as $show_table_row)
  {
   $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
  }
  $select_query = "SELECT * FROM " . $table . "";
  $statement = $connect->prepare($select_query);
  $statement->execute();
  $total_row = $statement->rowCount();

  for($count=0; $count<$total_row; $count++)
  {
   $single_result = $statement->fetch(PDO::FETCH_ASSOC);
   $table_column_array = array_keys($single_result);
   $table_value_array = array_values($single_result);
   $output .= "\nINSERT INTO $table (";
   $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
   $output .= "'" . implode("','", $table_value_array) . "');\n";
  }
 }
 $file_name = 'ujikom' . date('y-m-d') . '.sql';
 $file_handle = fopen($file_name, 'w+');
 fwrite($file_handle, $output);
 fclose($file_handle);
 header('Content-Description: File Transfer');
 header('Content-Type: application/octet-stream');
 header('Content-Disposition: attachment; filename=' . basename($file_name));
 header('Content-Transfer-Encoding: binary');
 header('Expires: 0');
 header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file_name));
    ob_clean();
    flush();
    readfile($file_name);
    unlink($file_name);
}

?>
<?php
include "header.php";
?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>

  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                        <div class="sparkline12-list" >
                            <div class="sparkline12-hd">
                            </div>
                            <div class="sparkline12-graph">
                                <div class="basic-login-form-ad">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="all-form-element-inner">
                                              <div class="container">
                                                <form method="post" id="export_form">
                                                  <h3>Pilih Tabel Untuk Export</h3>
                                                  <?php
                                                  foreach($result as $table)
                                                  {
                                                  ?>
                                                  <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <div class="i-checks pull-left">
                                                        <label for="filled-in-box">
                                                          <input type="checkbox" class="filled-in" id="filled-in-box" name="table[]" value="<?php echo $table["Tables_in_ujikom"]; ?>" checked="checked"> <i></i><?php echo $table["Tables_in_ujikom"]; ?></label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  <?php
                                                  }
                                                  ?>
                                                    <button type="submit" name="submit" id="submit" value="Export" class="btn btn-primary">Export Database</button>
                                                </form>
                                                    </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
</body>
<?php
include"footer.php";
?>
</html>