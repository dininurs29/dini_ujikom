<?php
include "header_o.php";
include "koneksi.php";
?>

    <!doctype html>
    <html class="no-js" lang="en">
    <head>
        <title>Ujikom | Dini Nursafitri</title>
    </head>

    <body>        

           <!-- batasan --> 
           <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Projects <span class="table-project-n">Data</span> Table</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <div id="toolbar">
                                        <select class="form-control dt-tb">
                                            <option value="">Export Basic</option>
                                            <option value="all">Export All</option>
                                            <option value="selected">Export Selected</option>
                                        </select>
                                    </div>
                                    <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                        <tr>
                                            <th >NO</th>
                                            <th >Nama Peminjam</th>
                                            <th >Nama Barang</th>
                                            <th >Tanggal Kembali</th>
                                            <th >Status</th>
                                            
                                        </tr>
                                    </thead>
                                        <tbody>
                                            <?php
                                            $nomer=1;
                                             $pilih=mysqli_query($konek,"SELECT * FROM peminjaman n join inventaris i on n.id_inventaris=i.id_inventaris join detail_pinjam d on n.id_peminjaman=d.id_peminjaman where n.status_peminjaman='Sudah Dikembalikan' order by n.tanggal_pengembalian desc");
                                              while($tampil=mysqli_fetch_array($pilih)){
                                            ?>
                                            <tr>
                                            <td><?php echo $nomer++;?></td>
                                            <td><?php echo $tampil['nm_peminjam'];?></td>
                                            <td><?php echo $tampil['nama'];?></td>
                                            <td><?php echo $tampil['tanggal_pengembalian'];?></td>
                                            <td><?php echo $tampil['status_peminjaman'];?></td>
                                            
                                            </tr>

                                            <?php
                                            }
                                        
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

</body>
    <?php
    include "footer_o.php";
    ?>
</html>