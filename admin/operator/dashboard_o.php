<?php
include "header_o.php";
include "koneksi.php";
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title></title>
</head>

<body>  
<!-- isi -->    
 <div class="product-sales-area mg-tb-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-sales-chart">
                            <div class="portlet-title">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="caption pro-sl-hd">
                                            <span class="caption-subject" style="margin-left: 250px;"><b>Smkn 1 Ciomas</b></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="actions graph-rp actions-graph-rp">
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <ul class="list-inline cus-product-sl-rp">
                                <li>
                                    <h5><i class="fa fa-circle" style="color: #006DF0;"></i>Inventaris</h5>
                                    <p>Inventaris atau inventarisasi barang adalah semua kegiatan dan usaha untuk memperoleh data yang diperlukan tentang ketersediaan barang-barang yang dimiliki dan diurus, baik yang diadakan melalui pembelian menggunakan anggaran belanja, maupun sumbangan atau hibah untuk diadministrasikan sebagaimana mestinya menurut ketentuan dan cara yang telah ditetapkan di masing-masing instansi.</p>
                                </li>
                                <li>
                                    <h5><i class="fa fa-circle" style="color: #933EC5;"></i>Sarana</h5>
                                    <p>segala sesuatu yang dapat dipakai sebagai alat dalam mencapai maksud atau tujuan. Sedangkan prasarana adalah segala sesuatu yang merupakan penunjang utama terselenggaranya suatu proses (usaha, pembangunan, proyek).</p>
                                </li>
                                <li>
                                    <h5><i class="fa fa-circle" style="color: #65b12d;"></i>Prasarana</h5>
                                    <p>alat penunjang keberhasilan suatu proses upaya yang dilakukan di dalam pelayanan publik, karena apabila kedua hal ini tidak tersedia maka semua kegiatan yang dilakukan tidak akan dapat mencapai hasil yang diharapkan sesuai dengan rencana.</p>
                                </li>
                            </ul>
                            <div id="morris-area-chart"></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="analysis-progrebar res-mg-t-30 mg-ub-10 res-mg-b-30 table-mg-t-pro-n tb-sm-res-d-n dk-res-t-d-n">
                            <div class="analysis-progrebar-content">
                                <h5>Alamat</h5>
                                    <p>JL. RAYA LALADON DESA LALADON KECAMATAN CIOMAS, Laladon, Kec. Ciomas, Kab. Bogor Prov. Jawa Barat</p>  
                            </div>
                        </div>
                        <div class="analysis-progrebar reso-mg-b-30 res-mg-b-30 mg-ub-10 tb-sm-res-d-n dk-res-t-d-n">
                            <div class="analysis-progrebar-content">
                                <h5>Contact</h5>
                                    <p>Telepon: 084040404<br>
                                       Email: smkn1ciomas@gmail.com
                                    </p>  
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
    </div> 

    <?php
    include "footer_o.php";
    ?>
</body>

</html>