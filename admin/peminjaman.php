<?php
include "header.php";
include 'koneksi.php';
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Ujikom | Dini Nursafitri</title>
</head>

<body>
    <!-- Tambah Data -->
    <div class="row">
        <div id="PrimaryModalhdbgcl" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-color-modal bg-color-1">
                        <h5 class="modal-title">Data Peminjaman</h5>
                        <div class="modal-close-area modal-close-df">
                            <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="sparkline12-graph">
                                <div class="basic-login-form-ad">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="all-form-element-inner">
                                                <form action="proses_peminjaman.php" method="POST" id="PrimaryModalhdbgcl">
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Nama Peminjam</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <input name="nm_peminjam" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Jenis</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <div class="chosen-select-single mg-b-20">
                                                                    <select class="form-control custom-select-value" name="id_inventaris">
                                                                            <option value="" disabled selected>Pilih</option>

                                                                            <?php
                                                                            $pilih=mysqli_query($konek, "SELECT * FROM inventaris");
                                                                            while($tampil=mysqli_fetch_array($pilih)){
                                                                            ?>
                                                                            <option value="<?=$tampil['id_inventaris'];?>"><?=$tampil['nama'];?></option>
                                                                              <?php
                                                                              }
                                                                              ?>
                                                                        </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Jumlah</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <input name="jumlah" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                <button type="submit" name="tambah" class="btn btn-primary">Tambah</button>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <!-- <div class="modal-footer">
                        <div class="modal-area-button">
                            </div>
                        
                    </div> -->
                </div>
            </div>
        </div>
    </div>


    <!-- Selesai Tambah Data -->
           <!-- batasan --> 
           <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px;">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                 <div class="sparkline13-hd">
                                <div class="modal-area-button">
                                <a class="Primary mg-b-10" href="#" data-toggle="modal" data-target="#PrimaryModalhdbgcl">Tambah</a>
                                
                            </div>
                            </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <div id="toolbar">
                                        <select class="form-control dt-tb">
                                            <option value="">Export Basic</option>
                                            <option value="all">Export All</option>
                                            <option value="selected">Export Selected</option>
                                        </select>
                                    </div>
                                    <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                        <tr>
                                            <th >NO</th>
                                            <th >Nama Peminjam</th>
                                            <th >Nama Barang</th>
                                            <th >Jumlah</th>
                                            <th >Tanggal Peminjaman</th>                                            
                                            <th >Status</th>
                                            <th data-field="action">Action</th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                            <?php
                                            $nomer=1;
                                             $pilih=mysqli_query($konek,"SELECT * FROM peminjaman n join inventaris i on n.id_inventaris=i.id_inventaris join detail_pinjam d on n.id_peminjaman=d.id_peminjaman WHERE n.status_peminjaman='Dipinjam'");
                                              while($tampil=mysqli_fetch_array($pilih)){
                                            ?>
                                            <tr>
                                            <td><?php echo $nomer++;?></td>
                                            <td><?php echo $tampil['nm_peminjam'];?></td>
                                            <td><?php echo $tampil['nama'];?></td>
                                            <td><?php echo $tampil['jumlah'];?></td>
                                            <td><?php echo $tampil['tanggal_peminjaman'];?></td>                                            
                                            <td><?php echo $tampil['status_peminjaman'];?></td>
                                            <td>
                                                <a href="proses_kembali.php?id_peminjaman=<?=$tampil['id_peminjaman'];?>&jumlah_d=<?=$tampil['jumlah']?>&id_inventaris=<?=$tampil['id_inventaris']?>"><button type="button" class="btn btn-success"><i class="fa fa-undo edu-graph-round" aria-hidden="true"></i></button></a>

                                                
                                                <a class="Warning Warning-color mg-b-10" href="#?nm_peminjam=<?=$tampil['nm_peminjam'];?>&jumlah=<?=$tampil['jumlah']?>&nama=<?=$tampil['nama']?>" data-toggle="modal" data-target="#WarningModalblbgpro-<?php echo $tampil['id_peminjaman']?>"><button type="button" class="btn btn-success"><i class="fa fa-eye edu-graph-round" aria-hidden="true"></i></button></a>
                                                
                                            </td>
                                            </tr>
                                                <!-- end modal ke 2 -->
     <div class="row">
        <div id="WarningModalblbgpro-<?php echo $tampil['id_peminjaman']?>" class="modal modal-edu-general Customwidth-popup-WarningModal PrimaryModal-bgcolor fade" role="dialog">
            <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-close-area modal-close-df">
                        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                        </div>
                        <div class="modal-body" style="color: white;">
                            <div class="row">
                                <h5>Nama Peminjam : <?php echo $tampil['nm_peminjam'];?></h5>
                            </div>
                            <div class="row">
                                <h5>Nama Barang : <?php echo $tampil['nama'];?></h5>
                            </div>
                            <div class="row">
                                <h5>Jumlah : <?php echo $tampil['jumlah'];?></h5>
                            </div>
                            <div class="row">
                                <h5>Tanggal Peminjaman : <?php echo $tampil['tanggal_peminjaman'];?></h5>
                            </div>
                            <div class="row">
                                <h5>Status : <?php echo $tampil['status_peminjaman'];?></h5>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

</body>
    <?php
    include "footer.php";
    ?>
</html>