<?php
include "header.php";
include 'koneksi.php';
?>
<!doctype html>
<html>
<head>
    <title>Ujikom | Dini Nursafitri</title>
</head>

<body> 
    <!-- tambah -->
        <div class="row">
        <div id="PrimaryModalhdbgcl" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-color-modal bg-color-1">
                        <h5 class="modal-title">Data Ruangan</h5>
                        <div class="modal-close-area modal-close-df">
                            <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="sparkline12-graph">
                                <div class="basic-login-form-ad">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="all-form-element-inner">
                                                <form action="proses_ruangan.php" method="POST">
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Nama Ruangan</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <input name="nama_ruang" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Kode</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <input name="kode_ruang" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro">Keterangan</label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <input name="keterangan" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="submit" name="tambah" class="btn btn-primary">Tambah</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                   <!--  <div class="modal-footer">
                        <a data-dismiss="modal" href="#">Cancel</a>
                        <button type="submit" name="tambah" class="btn btn-primary">Tambah</button>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
        <!-- end -->"
       <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px;">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="modal-area-button">
                                <a class="Primary mg-b-10" href="#" data-toggle="modal" data-target="#PrimaryModalhdbgcl">Tambah</a>
                                
                            </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <div id="toolbar">
                                        <select class="form-control dt-tb">
                                            <option value="">Export Basic</option>
                                            <option value="all">Export All</option>
                                            <option value="selected">Export Selected</option>
                                        </select>
                                    </div>
                                    <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                        <tr>
                                            
                                            <th data-field="">NO</th>
                                            <th data-field="nama_ruang" data-editable="true">Nama Ruangan</th>
                                            <th data-field="kode_ruang" data-editable="true">Kode</th>
                                            <th data-field="keterangan" data-editable="true">Keterangan</th>

                                            <th data-field="action">Action</th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                            <?php
                                            $nomer=1; 
                                            $pilih=mysqli_query($konek,"SELECT * FROM ruang");
                                            while($tampil=mysqli_fetch_array($pilih)){
                                            ?>
                                            <tr>
                                              <td><?=$nomer++;?></td>
                                              <td><?=$tampil['nama_ruang'];?></td>
                                              <td><?=$tampil['kode_ruang'];?></td>
                                              <td><?=$tampil['keterangan'];?></td>
                                              <td>
                                                <a href="hapus_ruangan.php?id=<?=$tampil['id_ruang'];?>"><button type="button" class="btn btn-danger"><i class="fa fa-trash edu-avatar" aria-hidden="true"></i></button></a>

                                                <a href="edit_ruangan.php?id=<?=$tampil['id_ruang'];?>"><button type="button" class="btn btn-primary"><i class="fa fa-pencil edu-graph-round" aria-hidden="true"></i></button></a>
                      
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>
    <?php
    include "footer.php";
    ?>
</html>