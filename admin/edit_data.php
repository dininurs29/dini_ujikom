<?php
include "header.php";
include "koneksi.php";
$edit=mysqli_query($konek,"SELECT * FROM inventaris WHERE id_inventaris='$_GET[id]'");
  while($tampil=mysqli_fetch_array($edit)){
?>

    <!doctype html>
    <html class="no-js" lang="en">
    <head>
        <title>Ujikom | Dini Nursafitri</title>
    </head>

    <body>        
        <div class="product-sales-area mg-tb-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                        <form action="" method="POST" id="PrimaryModalhdbgcl">
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Nama Barang</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <input name="nama" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" value="<?=$tampil['nama'];?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Kondisi</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <input name="kondisi" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" value="<?=$tampil['kondisi'];?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Keterangan</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <input name="keterangan" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" value="<?=$tampil['keterangan'];?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Jumlah</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <input name="jumlah" type="text" class="form-control" data-length="30" maxlength="30" autocomplete="off" required="" value="<?=$tampil['jumlah'];?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Jenis</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <div class="chosen-select-single mg-b-20">
                                            <select class="form-control custom-select-value" name="id_jenis" value="<?=$tampil['nama_jenis'];?>">
                                                <option value="" disabled selected>Pilih</option>

                                                <?php
                                                $pilih=mysqli_query($konek, "SELECT * FROM jenis");
                                                while($tampil1=mysqli_fetch_array($pilih)){
                                                    ?>
                                                    <option <?=($tampil['id_jenis']==$tampil1['id_jenis'])?"selected":'';?> value="<?=$tampil1['id_jenis'];?>"><?=$tampil1['nama_jenis'];?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Tanggal</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input name="tanggal_register" type="date"  class="form-control"  value="<?=$tampil['tanggal_register'];?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Ruangan</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <div class="chosen-select-single mg-b-20">
                                            <select class="form-control custom-select-value" name="id_ruang" value="<?=$tampil['nama_ruang'];?>">
                                                <option value="" disabled selected>Pilih</option>

                                                <?php
                                                $pilih=mysqli_query($konek, "SELECT * FROM ruang");
                                                while($tampil1=mysqli_fetch_array($pilih)){
                                                    ?>
                                                    <option <?=($tampil['id_ruang']==$tampil1['id_ruang'])?"selected":'';?> value="<?=$tampil1['id_ruang'];?>"><?=$tampil1['nama_ruang'];?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Kode</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <?php
                                        $koneksi=mysqli_connect("localhost","root","","ujikom");
                                        $cari_kd =mysqli_query($konek,"SELECT kode_inventaris from inventaris order by id_inventaris desc");
                                        $data = mysqli_fetch_array($cari_kd)
                                        ?>
                                        <input name="kode_inventaris" type="text" class="form-control" value="<?php echo substr($data['kode_inventaris'],4,1)+1?>" value="<?=$tampil['kode_inventaris'];?>" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Petugas</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <div class="chosen-select-single mg-b-20">
                                            <select class="form-control custom-select-value" name="id_petugas" value="<?=$tampil['petugas'];?>">
                                                <option value="" disabled selected>Pilih</option>

                                                <?php
                                                $pilih=mysqli_query($konek, "SELECT * FROM petugas");
                                                while($tampil1=mysqli_fetch_array($pilih)){
                                                    ?>
                                                    <option <?=($tampil['id_petugas']==$tampil1['id_petugas'])?"selected":'';?> value="<?=$tampil1['id_petugas'];?>"><?=$tampil1['nama_petugas'];?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" name="edit" class="btn btn-primary" style="margin-left: 780px;">Edit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div> 
        <?php
    }
    ?>

<?php
if(isset($_POST['edit'])){
$nama=$_POST['nama'];
$kondisi=$_POST['kondisi'];
$keterangan=$_POST['keterangan'];
$jumlah=$_POST['jumlah'];
$id_jenis=$_POST['id_jenis'];
$tanggal_register=$_POST['tanggal_register'];
$id_ruang=$_POST['id_ruang'];
$kode_inventaris=$_POST['kode_inventaris'];
$id_petugas=$_POST['id_petugas']; 

$edit=mysqli_query($konek,"UPDATE inventaris set nama ='$nama' , kondisi ='$kondisi' , keterangan ='$keterangan', jumlah ='$jumlah', id_jenis ='$id_jenis' , tanggal_register ='$tanggal_register', id_ruang ='$id_ruang', kode_inventaris ='DI00$kode_inventaris', id_petugas ='$id_petugas' WHERE id_inventaris='$_GET[id]'");
  if($edit){
   echo "<script>alert('Data Berhasil');window.location.href='data_barang.php';</script>";
  }else{
    echo"gagal";
  }
}

?>
</body>
<?php
include "footer.php";
?>

</html>